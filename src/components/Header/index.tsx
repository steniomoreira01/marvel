import { Content } from './styles';

import logomarvel from './../../assets/images/marvel.svg';

export default function Header() {
    return (
        <Content>
            <div className="content">
                <img src={logomarvel} alt="Marvel" />                
            </div>
        </Content>
    );
}