import { Link } from 'react-router-dom';
import { IoIosAddCircleOutline } from 'react-icons/io';

import { Article } from "./styles"

type CardProps = {
    id: string,
	name: string,
    link: string,
    thumbnail: {
        path: string;
        extension: string;
    }
}

export default function Card(props: CardProps){
    return (
        <Article>
            <Link to={`/${props.link}/${props.id}`}>
                <IoIosAddCircleOutline />
            </Link>
            <img src={`${props.thumbnail.path}.${props.thumbnail.extension}`} alt={props.name} />
            <h2>{props.name}</h2>
        </Article>
    )
}