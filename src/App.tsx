import Routes from './routes';

import './assets/styles/global.scss';

function App() {
  return (
    <Routes />
  )
}

export default App;