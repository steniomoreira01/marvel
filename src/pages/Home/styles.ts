import styled from 'styled-components';

export const Main = styled.main`
    .banner {
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;

        background-image: url('../../images/wallpaper-groot.png');    
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        
        min-height: 570px;
        padding: 0 15px;

        &::before {
            content: '';
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;            
            background: linear-gradient(to bottom right, rgba(0, 0, 0, .3), transparent 100%);
        }

        &::after {
            content: '';
            position: absolute;
            width: 100%;
            height: 100%;            
            left: 0;            
            bottom: 0;
            background: linear-gradient(to top, rgba(0, 0, 0, 2), transparent 100%);
        }

        .content {
            width: 100%;
            max-width: 1244px;
            z-index: 1;

            h1 {
                font-size: 5rem;
                text-transform: uppercase;
                color: var(--yellow);
                line-height: 1;
            }

            p {
                font-size:1.2rem;
                max-width: 700px;
                color: var(--white);
            }
        }
    }    

    .section-cards {
        background: linear-gradient(to bottom, var(--start-color), var(--end-color));
        padding: 0 15px 30px;

        form {
            padding: 20px 15px;
            text-align: center;
            margin-top: -100px;

            .input-block {
                position: relative;
                display: flex;
                justify-content: center;
                align-items: center;

                svg {
                    font-size: 1.5rem;
                }
            }

            .input-block input {
                width: 100%;
                max-width: 500px;
                font-size:1.2rem;
                color: var(--white);
                height: 2.9rem;   
                background: transparent;
                border: 0;
                border-bottom: 1px solid var(--white);
                outline: 0;
                padding: 0 1.6rem;
                margin-left: -20px;
            }
        }

        .cards-wrappers {
            position: relative;

            display: flex;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
            gap: 20px;                    
        }        
    }
`;