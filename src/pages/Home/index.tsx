import { useEffect, useState, FormEvent } from 'react';
import Header from "../../components/Header";
import { FiSearch } from 'react-icons/fi';
import Card from "../../components/Card";
import api from '../../services/api';

import { Main } from "./styles";

interface CardProps {
    id: string;
    name: string;
    link: string;
    thumbnail: {
        path: string;
        extension: string;
    }
}

export default function Home() {
    const [cards, setCards] = useState<CardProps[]>([]);
    const [character, setCharacter] = useState('');

    function handleOnSubmit(event: FormEvent) {
        event.preventDefault();
        api.get(`/characters${character && `?nameStartsWith=${character}`}`).then(
            response => {
                setCards(response.data.data.results);
            }
        );
    }

    useEffect(() => {
        api.get('/characters').then(
        response => {
            setCards(response.data.data.results);
        }
    )}, [])

    return (
        <>
            <Header />
            <Main>
                <div className="banner">
                    <div className="content">
                        <h1>Explore <br/>the Universe</h1>
                        <p>
                            Dive into the dazzling domain of all the classic 
                            characters you love - and those you’ll soon discover!
                        </p>
                    </div>
                </div>

                <div className="section-cards">
                    <form onSubmit={event => handleOnSubmit(event)}>
                        <div className="input-block">
                            <FiSearch />
                            <input
                                type="text"
                                name='character'
                                placeholder="Search | Eg: Groot"
                                value={character}
                                onChange={event => setCharacter(event.target.value)}
                            />                            
                        </div>
                    </form>
                    
                    <div className="cards-wrappers">
                        {cards.map(card => (
                            <Card
                                id={card.id}
                                name={card.name}
                                link='characters'
                                thumbnail={card.thumbnail}
                            />
                        ))}
                    </div>
                </div>
            </Main>
       </>
    );
}