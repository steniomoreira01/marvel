import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Card from '../../components/Card';
import Header from "../../components/Header";
import api from '../../services/api';
import Modal from './Modal';

import { Main } from "./styles";

interface Character {
    name: string;
    description: string;
    thumbnail: {
        path: string;
    }
    series: {
        items: Array<{
            resourceURI: string;
            name: string;
        }>
    }
}

interface Series {
    id: string,
    title: string;
    link: string;
    thumbnail: {
        path: string;
        extension: string;
    }
}

type CharacterParams = {
	id: string
};

export default function Details() {
    const [character, setCharacter] = useState<Character>();
    const [series, setSeries] = useState<Series[]>([]);
    const params = useParams<CharacterParams>();

    useEffect(() => {
        api.get(`/characters/${params.id}`).then(
            response => {
                setCharacter(response.data.data.results[0]);                              
            }
        );

        api.get(`/series?characters=${params.id}`).then(
            response => {
                setSeries(response.data.data.results);                              
            }
        );
    
    }, [params.id])

    if (!character) {
        return <p>Carregando...</p>
    }

    return (
        <>
            <Header/>
            <Main>
                <div className="topo">                
                    <img src={`${character.thumbnail.path}/detail.jpg`} alt={character.name} />

                    <div className="wrapper">                    
                        <h1>{character.name}</h1>
                        <p>{character.description}</p>

                        <button>To edit</button>
                    </div>
                </div>

                <div className="serie-list">
                    <h2>Series list</h2>
                    <div className="cards-wrappers">                    
                        {series.map(card => (
                            <Card
                                id={card.id}
                                name={card.title}
                                link='series'
                                thumbnail={card.thumbnail}
                            />
                        ))}
                    </div>
                </div>                
            </Main>
            <Modal />
        </>
    )
}