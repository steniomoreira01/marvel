import styled from 'styled-components';

export const Content = styled.div`
    position: fixed;
    top: 0;
    z-index: 2000;

    display:flex;
    align-items: center;
    justify-content: center;

    width: 100%;
    height: 100%;
    padding: 0 1rem;
    background-color: rgba(0,0,0,0.7);   

    opacity: 0;
    visibility: hidden;

    > .active {
        opacity: 1;
        visibility: visible;
    }

    .modal {
        background: var(--color-background);
        padding: 2.4rem;

        position: relative;
        z-index: 1;

        #form {
            max-width: 500px;
        }

        #form h2 {
            margin-top: 0;
        }

        input, textarea {
            border: none;
            border-radius: 0.2rem;

            padding: 0.8rem;
            width: 100%;
        }

        textarea {
            min-height: 130px;
        }

        .input-group {
            margin-top: 0.8rem;
        }

        .input-group.actions {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .input-group.actions .button,
        .input-group.actions button {
            padding: 10px;
            background: var(--yellow);
            border: 0;
            border-radius: .5rem;
            margin-top: 20px;
            cursor: pointer;
        }
    }
`;
