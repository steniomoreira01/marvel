import { Content } from "./styles";

export default function Modal() {
    return(
        <Content>
            <div className="modal">
                <div id="form">
                    <h2>Edit character</h2>
                    <form>
                        <div className="input-group">
                            <label 
                                className="sr-only" 
                                htmlFor="name">Name</label>
                            <input 
                                id="name" 
                                type="text" 
                                name="name"
                            />
                        </div>                       

                        <div className="input-group">
                            <label 
                                className="sr-only" 
                                htmlFor="description">Description</label>
                            <textarea 
                                id="description" 
                                name="description">                                    
                            </textarea>
                        </div>

                        <div className="input-group actions">
                            <span>Close</span>
                            <button>Salve</button>
                        </div>
                    </form>
                </div>
            </div>
        </Content>
    )
}